package com.innovenso.townplan.writer.archimate

import com.google.common.io.Files
import com.innovenso.eventsourcing.api.id.EntityId
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.repository.FileSystemAssetRepository
import spock.lang.Specification

class ArchimateWriterSpec extends Specification {
	def writePath = Files.createTempDir().getAbsolutePath()
	def assetRepository = new FileSystemAssetRepository(writePath, "https://test.com")
	def writer = new TownPlanOpenGroupExchangeWriter(assetRepository, writePath)
	def townPlan = new TownPlanImpl(new EntityId("test"))
	def samples = new SampleFactory(townPlan)

	def "enterprises are written to archimate"() {
		given:
		samples.enterprise()
		when:
		writer.write(townPlan)
		then:
		assetRepository.objectNames
		assetRepository.objectNames.each {println it}
		println writePath
	}
}
