package com.innovenso.townplan.writer.model.archimate;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.aspects.SWOTType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.deployment.aws.AwsInstance;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.domain.KeyPointInTimeImpl;
import com.innovenso.townplan.writer.model.SystemContainerDiagramModel;
import lombok.NonNull;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ArchimateTownPlanModel {
	private final TownPlan townPlan;

	public ArchimateTownPlanModel(@NonNull final TownPlan townPlan) {
		this.townPlan = townPlan;
	}

	public Set<BusinessCapability> getBusinessCapabilities() {
		return getAllBusinessCapabilities().filter(cap -> cap.getLevel() <= 2).collect(Collectors.toSet());
	}

	public Set<BusinessCapability> getLevelZeroBusinessCapabilities() {
		return getAllBusinessCapabilities().filter(cap -> cap.getLevel() == 0).collect(Collectors.toSet());
	}

	public Set<BusinessCapability> getLevelOneBusinessCapabilities() {
		return getAllBusinessCapabilities().filter(cap -> cap.getLevel() == 1).collect(Collectors.toSet());
	}

	public Set<ItPlatform> getPlatforms() {
		return townPlan.getElements(ItPlatform.class);
	}

	public Set<BusinessCapability> getLevelTwoBusinessCapabilities() {
		return getAllBusinessCapabilities().filter(cap -> cap.getLevel() == 2).collect(Collectors.toSet());
	}

	public Set<BusinessCapability> getBusinessFunctions() {
		return getAllBusinessCapabilities().filter(cap -> cap.getLevel() > 2).collect(Collectors.toSet());
	}

	public Stream<BusinessCapability> getAllBusinessCapabilities() {
		return townPlan.getElements(BusinessCapability.class).stream();
	}

	public Set<ArchitectureBuildingBlock> getBuildingBlocks() {
		return townPlan.getElements(ArchitectureBuildingBlock.class);
	}

	public Set<ItContainer> getContainers() {
		return townPlan.getElements(ItContainer.class);
	}

	public Set<BusinessActor> getAllActors() {
		return townPlan.getElements(BusinessActor.class);
	}

	public Set<DataEntity> getDataEntities() {
		return townPlan.getElements(DataEntity.class);
	}

	public Set<Relationship> getFlows() {
		return townPlan.getAllRelationships().stream()
				.filter(relation -> relation.getRelationshipType().equals(RelationshipType.FLOW))
				.collect(Collectors.toSet());
	}

	public Set<BusinessActor> getBusinessPeople() {
		return getAllActors().stream().filter(BusinessActor::isPerson).collect(Collectors.toSet());
	}

	public Set<BusinessActor> getSquads() {
		return getAllActors().stream().filter(BusinessActor::isSquad).collect(Collectors.toSet());
	}

	public Set<BusinessActor> getTribes() {
		return getAllActors().stream().filter(BusinessActor::isTribe).collect(Collectors.toSet());
	}

	public Set<BusinessActor> getAlliances() {
		return getAllActors().stream().filter(BusinessActor::isAlliance).collect(Collectors.toSet());
	}

	public Set<AwsInstance> getAwsInstances() {
		return townPlan.getElements(AwsInstance.class);
	}

	public Set<ItSystem> getSystems() {
		return townPlan.getElements(ItSystem.class);
	}

	public Set<ItSystemIntegration> getSystemIntegrations() {
		return townPlan.getElements(ItSystemIntegration.class);
	}

	public Set<ItProject> getProjects() {
		return townPlan.getElements(ItProject.class);
	}

	public Set<ItProjectMilestone> getMilestones() {
		return townPlan.getElements(ItProjectMilestone.class);
	}

	public Set<SWOTType> getSwotTypes() {
		return Set.of(SWOTType.values());
	}

	public String getTitle() {
		return townPlan.getElements(Enterprise.class).stream().map(Enterprise::getTitle)
				.collect(Collectors.joining(", "));
	}

	public String getDescription() {
		return townPlan.getElements(Enterprise.class).stream().map(Enterprise::getDescription)
				.collect(Collectors.joining("; "));
	}

	public Set<SystemContainerDiagramModel> getSystemContainerDiagramModels() {
		return townPlan.getElements(ItSystem.class).stream()
				.map(system -> new SystemContainerDiagramModel(system,
						KeyPointInTimeImpl.builder().date(LocalDate.now()).build(), townPlan))
				.collect(Collectors.toSet());
	}

}
