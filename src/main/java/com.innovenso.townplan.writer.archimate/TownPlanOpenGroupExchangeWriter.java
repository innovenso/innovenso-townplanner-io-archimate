package com.innovenso.townplan.writer.archimate;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.archimate.ArchimateTownPlanModel;
import com.innovenso.townplan.writer.pipeline.AbstractTownPlanWriter;
import com.innovenso.townplan.writer.pipeline.freemarker.FreemarkerRenderer;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;

@Log4j2
public class TownPlanOpenGroupExchangeWriter extends AbstractTownPlanWriter {
	private File targetBaseDirectory;
	private final FreemarkerRenderer freemarkerRenderer;

	public TownPlanOpenGroupExchangeWriter(@NonNull final AssetRepository assetRepository,
			@NonNull final String targetBasePath) {
		super(assetRepository, ContentOutputType.ARCHIMATE);
		setTargetBaseDirectory(new File(targetBasePath, "archimate"));
		this.freemarkerRenderer = new FreemarkerRenderer("archimate");
	}

	@Override
	protected void writeFiles(@NonNull TownPlan townPlan, @NonNull List<File> renderedFiles) {
		freemarkerRenderer.write(Map.of("element", new ArchimateTownPlanModel(townPlan)), "enterprise-town-plan.ftl")
				.ifPresent(render -> {
					final File resultingFile = new File(targetBaseDirectory,
							townPlan.getId().getEntityId().value + ".xml");
					try {
						FileUtils.copyFile(render, resultingFile);
						log.info("XML output file saved {}", resultingFile.getAbsolutePath());
						renderedFiles.add(resultingFile);
					} catch (IOException e) {
						log.error(e);
					}
				});
	}

	@Override
	protected void writeFiles(@NonNull TownPlan townPlan, @NonNull List<File> renderedFiles, @NonNull String s) {
		log.warn("Exporting a single concept to Archimate is not supported, exporting full town plan instead");
		writeFiles(townPlan, renderedFiles);
	}

	@Override
	public File getTargetBaseDirectory() {
		return targetBaseDirectory;
	}

	@Override
	public void setTargetBaseDirectory(@NonNull File targetBaseDirectory) {
		this.targetBaseDirectory = targetBaseDirectory;
		this.targetBaseDirectory.mkdirs();
	}
}
