<#ftl output_format="XML">
<?xml version="1.0" encoding="UTF-8"?>
<#import  "lib/system-container-diagram.ftl" as scd>
<#import "lib/aspects.ftl" as asp>
<model xmlns="http://www.opengroup.org/xsd/archimate/3.0/"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.opengroup.org/xsd/archimate/3.0/ http://www.opengroup.org/xsd/archimate/3.1/archimate3_Model.xsd"
       identifier="model-1"
       version="1.0">

    <name xml:lang="en">${element.title} Town Plan</name>

    <documentation xml:lang="en">${element.description!''}</documentation>

    <elements>
        <#list element.businessCapabilities as cap>
            <element identifier="${cap.camelCasedKey}" xsi:type="Capability">
                <name xml:lang="en">${cap.title}</name>
                <documentation xml:lang="en">${cap.description!''}</documentation>
                <@asp.aspects concept=cap />
            </element>
        </#list>
        <#list element.businessFunctions as cap>
            <element identifier="${cap.camelCasedKey}" xsi:type="BusinessFunction">
                <name xml:lang="en">${cap.title}</name>
                <documentation xml:lang="en">${cap.description!''}</documentation>
                <@asp.aspects concept=cap />
            </element>
        </#list>
        <#list element.buildingBlocks as block>
            <element identifier="${block.camelCasedKey}" xsi:type="ApplicationFunction">
                <name xml:lang="en">${block.title}</name>
                <documentation xml:lang="en">${block.description!''}</documentation>
                <@asp.aspects concept=block />
            </element>
        </#list>
        <#list element.platforms as platform>
            <element identifier="${platform.camelCasedKey}" xsi:type="ApplicationComponent">
                <name xml:lang="en">${platform.title}</name>
                <documentation xml:lang="en">${platform.description!''}</documentation>
                <@asp.aspects concept=platform />
            </element>
        </#list>

        <#list element.systems as system>
            <element identifier="${system.camelCasedKey}" xsi:type="ApplicationComponent">
                <name xml:lang="en">${system.title}</name>
                <documentation xml:lang="en">${system.description!''}</documentation>
                <@asp.aspects concept=system />
            </element>
        </#list>
        <#list element.systemIntegrations as integration>
            <element identifier="${integration.camelCasedKey}" xsi:type="ApplicationInteraction">
                <name xml:lang="en">${integration.title}</name>
                <documentation xml:lang="en">${integration.description!''}</documentation>
                <@asp.aspects concept=integration />
            </element>
        </#list>
        <#list element.containers as container>
            <element identifier="${container.camelCasedKey}" xsi:type="ApplicationComponent">
                <name xml:lang="en">${container.title}</name>
                <documentation xml:lang="en">${container.description!''}</documentation>
                <@asp.aspects concept=container />
            </element>
        </#list>

        <#list element.allActors as actor>
            <element identifier="${actor.camelCasedKey}" xsi:type="BusinessActor">
                <name xml:lang="en">${actor.title}</name>
                <documentation xml:lang="en">${actor.description!''}</documentation>
                <@asp.aspects concept=actor />
            </element>
        </#list>

        <#list element.dataEntities as entity>
            <element identifier="${entity.camelCasedKey}" xsi:type="DataObject">
                <name xml:lang="en">${entity.title}</name>
                <documentation xml:lang="en">${entity.description!''}</documentation>
                <@asp.aspects concept=entity />
            </element>
        </#list>

        <#list element.projects as project>
            <element identifier="${project.camelCasedKey}" xsi:type="WorkPackage">
                <name xml:lang="en">${project.title}</name>
                <documentation xml:lang="en">${project.description!''}</documentation>
                <@asp.aspects concept=project />
            </element>
        </#list>

        <#list element.milestones as milestone>
            <element identifier="${milestone.camelCasedKey}" xsi:type="Plateau">
                <name xml:lang="en">${milestone.title}</name>
                <documentation xml:lang="en">${milestone.description!''}</documentation>
                <@asp.aspects concept=milestone />
            </element>
        </#list>

        <#list element.awsInstances as instance>
            <element identifier="${instance.camelCasedKey}" xsi:type="SystemSoftware">
                <name xml:lang="en">${instance.title}</name>
                <documentation xml:lang="en">${instance.description!''}</documentation>
            </element>
        </#list>
    </elements>

    <relationships>
        <#list element.businessCapabilities as cap>
            <#list cap.childCapabilities as childCap>
                <relationship identifier="${childCap.camelCasedKey}_belongsto_${cap.camelCasedKey}" source="${childCap.camelCasedKey}" target="${cap.camelCasedKey}" xsi:type="Composition"/>
            </#list>
        </#list>
        <#list element.buildingBlocks as block>
            <#list block.realizedCapabilities as cap>
                <relationship identifier="${block.camelCasedKey}_realizes_${cap.camelCasedKey}" source="${block.camelCasedKey}" target="${cap.camelCasedKey}" xsi:type="Realization">
                    <name xml:lang="en">realizes</name>
                </relationship>
            </#list>
        </#list>
        <#list element.systems as system>
            <#list system.buildingBlocks as block>
                <relationship identifier="${system.camelCasedKey}_realizes_${block.camelCasedKey}" source="${system.camelCasedKey}" target="${block.camelCasedKey}" xsi:type="Realization">
                    <name xml:lang="en">realizes</name>
                </relationship>
            </#list>
        </#list>
        <#list element.systemIntegrations as integration>
            <relationship identifier="${integration.sourceSystem.camelCasedKey}_integration_source_${integration.camelCasedKey}" source="${integration.sourceSystem.camelCasedKey}" target="${integration.camelCasedKey}" xsi:type="Association">
                <name xml:lang="en">integrates</name>
            </relationship>
            <relationship identifier="${integration.targetSystem.camelCasedKey}_integration_target_${integration.camelCasedKey}" source="${integration.targetSystem.camelCasedKey}" target="${integration.camelCasedKey}" xsi:type="Association">
                <name xml:lang="en">integrates</name>
            </relationship>
        </#list>

        <#list element.containers as container>
            <relationship identifier="${container.camelCasedKey}_belongsto_${container.system.camelCasedKey}" source="${container.camelCasedKey}" target="${container.system.camelCasedKey}" xsi:type="Composition"/>
        </#list>

        <#list element.allActors as actor>
            <#list actor.getMembers() as member>
                <relationship identifier="${member.camelCasedKey}_belongsto_${actor.camelCasedKey}" source="${member.camelCasedKey}" target="${actor.camelCasedKey}" xsi:type="Composition">
                    <name xml:lang="en">is member of</name>
                </relationship>
            </#list>
        </#list>
        <#list element.flows as flow>
            <relationship identifier="${flow.camelCasedKey}" source="${flow.source.camelCasedKey}" target="${flow.target.camelCasedKey}" xsi:type="Flow">
                <name xml:lang="en">${flow.title}</name>
                <documentation xml:lang="en">${flow.description!''}</documentation>
            </relationship>
        </#list>

        <#list element.projects as project>
            <#list project.milestones as milestone>
                <relationship identifier="${project.camelCasedKey}_realizes_${milestone.camelCasedKey}" source="${project.camelCasedKey}" target="${milestone.camelCasedKey}" xsi:type="Realization">
                    <name xml:lang="en">realizes</name>
                </relationship>
            </#list>
            <#list project.stakeholderRelationships as relationship>
                <relationship identifier="${relationship.camelCasedKey}" source="${relationship.source.camelCasedKey}" target="${relationship.target.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">${relationship.title}</name>
                </relationship>
            </#list>
        </#list>
        <#list element.milestones as milestone>
            <#list milestone.addedBusinessCapabilities as impacted>
                <relationship identifier="${milestone.camelCasedKey}_adds_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">adds</name>
                </relationship>
            </#list>
            <#list milestone.addedBuildingBlocks as impacted>
                <relationship identifier="${milestone.camelCasedKey}_adds_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">adds</name>
                </relationship>
            </#list>
            <#list milestone.addedItSystems as impacted>
                <relationship identifier="${milestone.camelCasedKey}_adds_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">adds</name>
                </relationship>
            </#list>
            <#list milestone.addedItSystemIntegrations as impacted>
                <relationship identifier="${milestone.camelCasedKey}_adds_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">adds</name>
                </relationship>
            </#list>
            <#list milestone.addedItContainers as impacted>
                <relationship identifier="${milestone.camelCasedKey}_adds_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">adds</name>
                </relationship>
            </#list>

            <#list milestone.changedBusinessCapabilities as impacted>
                <relationship identifier="${milestone.camelCasedKey}_changes_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">changes</name>
                </relationship>
            </#list>
            <#list milestone.changedBuildingBlocks as impacted>
                <relationship identifier="${milestone.camelCasedKey}_changes_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">changes</name>
                </relationship>
            </#list>
            <#list milestone.changedItSystems as impacted>
                <relationship identifier="${milestone.camelCasedKey}_changes_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">changes</name>
                </relationship>
            </#list>
            <#list milestone.changedItSystemIntegrations as impacted>
                <relationship identifier="${milestone.camelCasedKey}_changes_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">changes</name>
                </relationship>
            </#list>
            <#list milestone.changedItContainers as impacted>
                <relationship identifier="${milestone.camelCasedKey}_changes_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">changes</name>
                </relationship>
            </#list>

            <#list milestone.removedBusinessCapabilities as impacted>
                <relationship identifier="${milestone.camelCasedKey}_removes_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">removes</name>
                </relationship>
            </#list>
            <#list milestone.removedBuildingBlocks as impacted>
                <relationship identifier="${milestone.camelCasedKey}_removes_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">removes</name>
                </relationship>
            </#list>
            <#list milestone.removedItSystems as impacted>
                <relationship identifier="${milestone.camelCasedKey}_removes_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">removes</name>
                </relationship>
            </#list>
            <#list milestone.removedItSystemIntegrations as impacted>
                <relationship identifier="${milestone.camelCasedKey}_removes_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">removes</name>
                </relationship>
            </#list>
            <#list milestone.removedItContainers as impacted>
                <relationship identifier="${milestone.camelCasedKey}_removes_${impacted.camelCasedKey}" source="${milestone.camelCasedKey}" target="${impacted.camelCasedKey}" xsi:type="Association" isDirected="true">
                    <name xml:lang="en">removes</name>
                </relationship>
            </#list>
        </#list>
    </relationships>


    <organizations>
        <item>
            <label xml:lang="en">Strategy</label>
            <item>
                <label xml:lang="en">Business Capabilities</label>
                <item>
                    <label xml:lang="en">Level 0</label>
                    <#list element.levelZeroBusinessCapabilities as cap>
                        <item identifierRef="${cap.camelCasedKey}" />
                    </#list>
                </item>
                <item>
                    <label xml:lang="en">Level 1</label>
                    <#list element.levelOneBusinessCapabilities as cap>
                        <item identifierRef="${cap.camelCasedKey}" />
                    </#list>
                </item>
                <item>
                    <label xml:lang="en">Level 2</label>
                    <#list element.levelTwoBusinessCapabilities as cap>
                        <item identifierRef="${cap.camelCasedKey}" />
                    </#list>
                </item>
            </item>
        </item>
        <item>
            <label xml:lang="en">Business</label>
            <item>
                <label xml:lang="en">Business Functions</label>
                <#list element.businessFunctions as cap>
                    <item identifierRef="${cap.camelCasedKey}" />
                </#list>
            </item>
            <item>
                <label xml:lang="en">People</label>
                <#list element.businessPeople as actor>
                    <item identifierRef="${actor.camelCasedKey}" />
                </#list>
            </item>
            <item>
                <label xml:lang="en">Squads</label>
                <#list element.squads as actor>
                    <item identifierRef="${actor.camelCasedKey}" />
                </#list>
            </item>
            <item>
                <label xml:lang="en">Tribes</label>
                <#list element.tribes as actor>
                    <item identifierRef="${actor.camelCasedKey}" />
                </#list>
            </item>
            <item>
                <label xml:lang="en">Alliances</label>
                <#list element.alliances as actor>
                    <item identifierRef="${actor.camelCasedKey}" />
                </#list>
            </item>
        </item>
        <item>
            <label xml:lang="en">Application</label>
            <item>
                <label xml:lang="en">Solution Building Blocks</label>
                <#list element.buildingBlocks as block>
                    <item identifierRef="${block.camelCasedKey}" />
                </#list>
            </item>
            <item>
                <label xml:lang="en">Platforms</label>
                <#list element.platforms as platform>
                    <item identifierRef="${platform.camelCasedKey}" />
                </#list>
            </item>
            <item>
                <label xml:lang="en">Systems</label>
                <#list element.systems as system>
                    <item identifierRef="${system.camelCasedKey}" />
                </#list>
            </item>
            <item>
                <label xml:lang="en">System Integrations</label>
                <#list element.systemIntegrations as integration>
                    <item identifierRef="${integration.camelCasedKey}" />
                </#list>
            </item>
            <item>
                <label xml:lang="en">Containers</label>
                <#list element.containers as container>
                    <item identifierRef="${container.camelCasedKey}" />
                </#list>
            </item>
            <item>
                <label xml:lang="en">Information Model</label>
                <#list element.dataEntities as entity>
                    <item identifierRef="${entity.camelCasedKey}" />
                </#list>
            </item>
        </item>
        <item>
            <label xml:lang="en">Implementation &amp; Migration</label>
            <item>
                <label xml:lang="en">Projects</label>
                <#list element.projects as project>
                    <item identifierRef="${project.camelCasedKey}" />
                </#list>
            </item>
            <item>
                <label xml:lang="en">Plateaus</label>
                <#list element.milestones as milestone>
                    <item identifierRef="${milestone.camelCasedKey}" />
                </#list>
            </item>
        </item>
        <item>
            <label xml:lang="en">Views</label>
            <item>
                <label xml:lang="en">System Container Diagrams</label>
                <#list element.systemContainerDiagramModels as model>
                    <item identifierRef="${model.centralSystem.camelCasedKey}_system_container_diagram" />
                </#list>
            </item>
        </item>
    </organizations>

    <propertyDefinitions>
        <propertyDefinition identifier="documentation-property" type="string">
            <name>documentation</name>
        </propertyDefinition>
        <propertyDefinition identifier="lifecycle-property" type="string">
            <name>lifecycle</name>
        </propertyDefinition>
        <propertyDefinition identifier="verdict-property" type="string">
            <name>architecture verdict</name>
        </propertyDefinition>
        <#list element.swotTypes as swotType>
            <propertyDefinition identifier="${swotType.value?lower_case}-property" type="string">
                <name>${swotType.label}</name>
            </propertyDefinition>
        </#list>
    </propertyDefinitions>
    <views>
        <diagrams>
            <#list element.systemContainerDiagramModels as model>
                <@scd.system_container_diagram model=model />
            </#list>
        </diagrams>
    </views>



</model>
