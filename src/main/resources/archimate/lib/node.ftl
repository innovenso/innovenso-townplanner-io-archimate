<#ftl output_format="XML">
<#macro context element viewId col row>
<node identifier="${viewId}_${element.camelCasedKey}" elementRef="${element.camelCasedKey}" xsi:type="Element" x="${(400 * col)?c}" y="${(150 * row)?c}" w="500" h="250">
    <style>
        <fillColor r="255" g="255" b="255" a="100"/>
        <lineColor r="92" g="92" b="92" a="100"/>
        <font name="Lucida Grande" size="12"><color r="0" g="0" b="0"/></font>
    </style>
    <#nested>
</node>
</#macro>

<#macro system element viewId col row>
    <node identifier="${viewId}_${element.camelCasedKey}" elementRef="${element.camelCasedKey}" xsi:type="Element" x="${(140 * col)?c}" y="${(150 * row)?c}" w="120" h="55">
        <style>
            <fillColor r="181" g="255" b="255" a="100"/>
            <lineColor r="92" g="92" b="92" a="100"/>
            <font name="Lucida Grande" size="12"><color r="0" g="0" b="0"/></font>
        </style>
    </node>
</#macro>

<#macro integration element viewId col row>
    <node identifier="${viewId}_${element.camelCasedKey}" elementRef="${element.camelCasedKey}" xsi:type="Element" x="${(140 * col)?c}" y="${(150 * row)?c}" w="120" h="55">
        <style>
            <fillColor r="181" g="255" b="255" a="100"/>
                                                       <lineColor r="92" g="92" b="92" a="100"/>
                                                                                               <font name="Lucida Grande" size="12"><color r="0" g="0" b="0"/></font>
        </style>
    </node>
</#macro>

<#macro container element viewId col row>
    <node identifier="${viewId}_${element.camelCasedKey}" elementRef="${element.camelCasedKey}" xsi:type="Element" x="${(140 * col)?c}" y="${(150 * row)?c}" w="120" h="55">
        <style>
            <fillColor r="176" g="196" b="222" a="100"/>
                                                       <lineColor r="92" g="92" b="92" a="100"/>
                                                                                               <font name="Lucida Grande" size="12"><color r="0" g="0" b="0"/></font>
        </style>
    </node>
</#macro>

<#macro actor element viewId col row>
    <node identifier="${viewId}_${element.camelCasedKey}" elementRef="${element.camelCasedKey}" xsi:type="Element" x="${(140 * col)?c}" y="${(150 * row)?c}" w="120" h="55">
        <style>
            <fillColor r="181" g="255" b="181" a="100"/>
            <lineColor r="92" g="92" b="92" a="100"/>
            <font name="Lucida Grande" size="12"><color r="0" g="0" b="0"/></font>
        </style>
    </node>
</#macro>

<#macro connection relationship viewId>
    <connection identifier="${viewId}_${relationship.camelCasedKey}" relationshipRef="${relationship.camelCasedKey}" xsi:type="Relationship" source="${viewId}_${relationship.source.camelCasedKey}" target="${viewId}_${relationship.target.camelCasedKey}">
        <style>
            <lineColor r="0" g="0" b="0"/>
            <font name="Lucida Grande" size="12"><color r="0" g="0" b="0"/></font>
        </style>
    </connection>
</#macro>


<#macro flowStep flowView step viewId counter>
    <connection identifier="${viewId}_${step.relationship.camelCasedKey}_${counter}" relationshipRef="${flowView.camelCasedKey}_step_${counter}" xsi:type="Relationship" source="${viewId}_${step.relationship.source.camelCasedKey}" target="${viewId}_${step.relationship.target.camelCasedKey}">
        <style>
            <lineColor r="0" g="0" b="0"/>
                                         <font name="Lucida Grande" size="12"><color r="0" g="0" b="0"/></font>
        </style>
    </connection>
</#macro>

