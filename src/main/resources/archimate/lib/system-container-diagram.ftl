<#ftl output_format="XML">
<#import  "node.ftl" as n>

<#macro system_container_diagram model>

    <view identifier="${model.centralSystem.camelCasedKey}_system_container_diagram" xsi:type="Diagram">
        <name xml:lang="en">${model.centralSystem.title}</name>

        <#if model.hasContainers()>
            <@n.context element=model.centralSystem viewId="${model.centralSystem.camelCasedKey}_system_container_diagram" col=0 row=0>
                <#list model.centralSystem.containers as container>
                    <@n.container element=container viewId="${model.centralSystem.camelCasedKey}_system_container_diagram" col=container?index row=0/>
                </#list>
            </@n.context>

        <#else>
            <@n.system element=model.centralSystem viewId="${model.centralSystem.camelCasedKey}_system_container_diagram" col=0 row=0/>
        </#if>

        <#-- render all the systems that interact with the system itself or any of the containers inside this system -->
        <#list model.systems as system>
            <@n.system element=system viewId="${model.centralSystem.camelCasedKey}_system_container_diagram" col=system?index row=1/>
        </#list>

        <#-- render all the actors that interact with the system itself or any of the containers inside this system -->
        <#list model.actors as actor>
            <@n.actor element=actor viewId="${model.centralSystem.camelCasedKey}_system_container_diagram" col=actor?index row=2/>
        </#list>

        <#-- render all relationships -->
        <#list model.flows as relationship>
            <@n.connection relationship=relationship viewId="${model.centralSystem.camelCasedKey}_system_container_diagram" />
        </#list>


    </view>
</#macro>