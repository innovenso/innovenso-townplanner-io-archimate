<#ftl output_format="XML">

<#macro aspects concept>
    <properties>
        <property propertyDefinitionRef="lifecycle-property">
            <value xml:lang="nl">${concept.lifecycle.type.value}</value>
        </property>
        <property propertyDefinitionRef="verdict-property">
            <value xml:lang="nl">${concept.architectureVerdict.verdictType.value}</value>
        </property>
         <#list concept.documentations as documentation>
            <property propertyDefinitionRef="documentation-property">
                <value xml:lang="nl">${documentation.description}</value>
            </property>
        </#list>
        <#list concept.swots as swot>
            <property propertyDefinitionRef="${swot.type?lower_case}-property">
                <value xml:lang="nl">${swot.description}</value>
            </property>
        </#list>
    </properties>

</#macro>